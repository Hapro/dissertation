//  Author: Mikiel Agutu (except where otherwise explicitly stated)

import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import InitialState from './InitialState';
import RootReducer from './reducers/RootReducer';

const Store = applyMiddleware(thunk)(createStore)(RootReducer, InitialState, applyMiddleware(thunk), window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

export default Store;