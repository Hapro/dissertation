//  Author: Mikiel Agutu (except where otherwise explicitly stated)

import Actions from './Actions';
import { AppStates } from '../InitialState';

function rootReducer(state, action){
    switch(action.type){
        case Actions.INIT_AR_DISPLAY:
            return {
                ...state,
                arDisplayData: {
                    ready: true,
                    displayWidth: action.displayWidth,
                    displayHeight: action.displayHeight
                }
            };

        case Actions.INIT_AR_3D_SCENE:
            return {
                ...state,
                ar3DScene: action.ar3DScene
            };

        case Actions.INIT_AR_ENGINE:
            return {
                ...state,
                arEngine: action.arEngine
            };

        case Actions.INIT_WEBCAM_CAPTURE:
            return {
                ...state,
                webcamCapture: action.webcamCapture
            };

        case Actions.SYNC_VIEW_WITH_AR_ENGINE:
            const newSyncValue = state.arEngineSync + 1;
            return { ...state, arEngineSync: newSyncValue }

        case Actions.VIDEO_SOURCE_SELECTED:
            return { ...state, appState: AppStates.JOIN_OR_START_GAME }

        case Actions.GAME_SERVER_STARTED:
            return { ...state, appState: AppStates.SETUP_GAME }

        case Actions.SET_GAME_SERVER_PASSWORD:
            return { ...state, gameServerPassword: action.gameServerPassword }

        case Actions.GAME_SET_UP:
            return { ...state, appState: AppStates.MAIN_STATE }

        case Actions.ADD_MODEL:
            const { modelToAdd } = action;

            if(state.models.length < 32) return { ...state, models: state.models.concat(action.model) };

            return state;

        case Actions.REMOVE_MODEL:
            return { ...state, models: state.models.filter((m) => { return m.id !== action.id }) }

        case Actions.JOIN_GAME_FAILED:
            return { ...state, appState: AppStates.JOIN_GAME_REFUSED }

        case Actions.ERROR:
            return { ...state, error: action.error }

        case Actions.HIGHLIGHT_AR_OBJECT:
            //If we mouse over nothing unhighlight whatever was highlighted
            if(action.arObject === null || action.arObject === undefined){
                if(state.highlightedArObject != null && state.highlightedArObject != undefined){
                    if(state.highlightedArObject !== state.arObject){ 
                        state.highlightedArObject.onUnSelect();
                    }
                }
                return {...state};
            }

            //If we have moused over something, highlight it
            if(action.arObject != null && action.arObject != undefined) action.arObject.onSelect();

            return { ...state, highlightedArObject: action.arObject }
        
        case Actions.SELECT_AR_OBJECT:
            //If we click nothing, unhighlight previous if there was one
            if(action.arObject === null || action.arObject === undefined){
                if(state.arObject != null && state.arObject != undefined) state.arObject.onUnSelect();
                return {...state};
            }

            //If we have moused over something, highlight it
            if(action.arObject != null && action.arObject != undefined) action.arObject.onSelect();
            
            return { ...state, arObject: action.arObject }
    }

    return {...state}
}


export default rootReducer;