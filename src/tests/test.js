//  Author: Mikiel Agutu (except where otherwise explicitly stated)

import React from "react";
import ReactDOM from "react-dom";
import { StaticString } from '../gui/Inputs';
import { shallow } from 'enzyme';
import { ControlLabel } from 'react-bootstrap';

describe("StaticString", function() {

    it("Displays expected information", function() {
        const result = shallow(<StaticString title={'foo'} value={'bar'} />);
        expect(result.find(ControlLabel).length).toBe(1);
    });
});
    