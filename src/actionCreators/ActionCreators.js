//  Author: Mikiel Agutu (except where otherwise explicitly stated)

import Actions from '../reducers/Actions';
import Socket from '../lib/socket';

//Action creators for Redux action
//Actions are used to alter the state of the appication
//i.e. dispatching a 'initializeArEngine' will initialize the AR engine in the
//Redux state of the application. Action creators are convenience functions
//for creating these actions

//Set up the game
function gameSetup(dispatch, models, ar3DScene){
    let modelsLoaded = 0;
    
    //Iteerate through each model
    models.forEach((model, i) => {
        //Add the model to the tracking system
        ar3DScene.addObjectToArTrackingSystem(i,
            model,
            (arObject3D) => {
                arObject3D.setColour(model.colour);
                modelsLoaded++;
                //Dispatch action when all models loaded
                if(modelsLoaded === models.length) dispatch({ type: Actions.GAME_SET_UP});
            },
            (error) => {
                dispatch({ type: Actions.ERROR, error: error });
            });
    });
}

const ActionCreators = {
    //Initialize the AR display
    initializeArDisplay: (displayWidth, displayHeight) => {
        return {
            type: Actions.INIT_AR_DISPLAY,
            displayWidth,
            displayHeight
        }
    },

    initializeArEngine: (arEngine) => {
        return{
            type: Actions.INIT_AR_ENGINE,
            arEngine
        }
    },

    initializeAr3DScene: (ar3DScene) => {
        return{
            type: Actions.INIT_AR_3D_SCENE,
            ar3DScene
        }
    },

    initializeWebcamCapture: (webcamCapture) => {
        return{
            type: Actions.INIT_WEBCAM_CAPTURE,
            webcamCapture
        }
    },

    syncViewWithArEngine: () => {
        return{
            type: Actions.SYNC_VIEW_WITH_AR_ENGINE
        }
    },

    videoSourceSelected: () => {
        return {
            type: Actions.VIDEO_SOURCE_SELECTED
        }
    },

    selectArObject: (arObject) =>{
        return {
            type: Actions.SELECT_AR_OBJECT,
            arObject
        }
    },

    //Join game using Socket.io
    joinGame: (ar3DScene, password) => {
        //Return an action creator
        return function(dispatch){
            //Emit 'join game' socket request
            Socket.emit('joinArGame', {password: password});

            //Whenw e get the reply
            Socket.on('joinArGameResponse', function(data){

                //If data was sent create the game
                if(data != null){
                    gameSetup(dispatch, data.models, ar3DScene);
                    dispatch({ type: Actions.SET_GAME_SERVER_PASSWORD, password: password });
                }
                else{
                    dispatch({ type: Actions.JOIN_GAME_FAILED });
                }
            });
        }
    },

    startGameServer: ()=> {
        return {
            type: Actions.GAME_SERVER_STARTED
        }
    },

    gameSetUp: (models, ar3DScene) => {
        return function(dispatch){

            //Emit a start game request
            Socket.emit('startArGame', {models: models});
            
            //Set up the game when the server has responded
            Socket.on('startArGameResponse', function(data){
                gameSetup(dispatch, models, ar3DScene);
                dispatch({ type: Actions.SET_GAME_SERVER_PASSWORD, gameServerPassword: data.password });
            });
        }
    },

    addModel: (model) => {
        const modelToAdd = {
            ...model,
            id: new Date()
        };

        return {
            type: Actions.ADD_MODEL,
            model: modelToAdd
        }
    },

    removeModel: (id) => {
        return{
            type: Actions.REMOVE_MODEL,
            id
        }
    },

    error: (error) => {
        return{
            type: Actions.ERROR,
            error
        }
    },

    setArObject: (arObject) => {
        return{
            type: Actions.SELECT_AR_OBJECT,
            arObject
        }
    },

    highlightArObject: (arObject) => {
        return{
            type: Actions.HIGHLIGHT_AR_OBJECT,
            arObject
        }
    }
};

export default ActionCreators;