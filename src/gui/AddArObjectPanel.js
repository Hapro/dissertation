//  Author: Mikiel Agutu (except where otherwise explicitly stated)

import React from 'react';
import { Button, ButtonToolbar, ButtonGroup, ControlLabel, Panel } from 'react-bootstrap';
import Add3DArObjectPanel from './Add3DArObjectPanel';
import AddImageARObjectPanel from './AddImageARObjectPanel';
import AddCustomArObjectPanel from './AddCustomArObjectPanel';
import InfoButton from './InfoButton';

const ItemToAdd = {
    Model: 'Model',
    Image: 'Image',
    CustomModel: 'Custom Model'
}

//Component for adding AR objects i.e. 3D models, web links etc
class AddArObjectPanel extends React.Component{
    constructor(props){
        super(props);
        this.state = { itemToAdd:  ItemToAdd.Model};

        this.setItemToAdd = this.setItemToAdd.bind(this);
    }

    setItemToAdd(item){
        this.setState({ itemToAdd: item })
    }

    render(){
        const infoText = 'Select the objects you wish to include in the AR scene with the dropdown box.';
        const { itemToAdd } = this.state;

        const panelDisplay = () => {
            switch(itemToAdd){
                case ItemToAdd.Image:
                    return <AddImageARObjectPanel/>
                case ItemToAdd.Model:
                    return <Add3DArObjectPanel/>
                case ItemToAdd.CustomModel:
                    return <AddCustomArObjectPanel/>
            }
        };

        return(
            <Panel>
            <h3>AR Scene creator <InfoButton text={infoText}/></h3>
            <ButtonToolbar>
                <ButtonGroup>
                <Button onClick={() => {this.setItemToAdd(ItemToAdd.Model)}}>3D Model</Button>
                <Button onClick={() => {this.setItemToAdd(ItemToAdd.Image)}}>Image upload</Button>
                <Button onClick={() => {this.setItemToAdd(ItemToAdd.CustomModel)}}>Custom 3D Model</Button>
            </ButtonGroup>
            </ButtonToolbar>
                {panelDisplay()}
            </Panel>
        );
    }
}

export default AddArObjectPanel;
