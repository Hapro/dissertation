//  Author: Mikiel Agutu (except where otherwise explicitly stated)

import React from 'react';
import ReactDOM from 'react-dom';

//Component for displaying an Ar3DScene object
class Ar3DSceneCanvas extends React.Component{
    componentDidMount(){
        const { ar3DScene } = this.props;
        const renderer = ar3DScene.ThreeRenderer();
        
        ReactDOM.findDOMNode(this.refs.rendererWrapper).appendChild(renderer.domElement);
    }

    render(){
        const { style } = this.props;
        return(<div style={style} ref='rendererWrapper'></div> );
    }
}

export default Ar3DSceneCanvas;