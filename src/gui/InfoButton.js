//  Author: Mikiel Agutu (except where otherwise explicitly stated)

import React from 'react';
import { Tooltip, Badge, OverlayTrigger } from 'react-bootstrap';

const InfoButton = ({text}) => {
    return (
        <OverlayTrigger placement="top" overlay={<Tooltip id="tooltip">{text}</Tooltip>}>
            <Badge>?</Badge>
        </OverlayTrigger>
    );
}

export default InfoButton;