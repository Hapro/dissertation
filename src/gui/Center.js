//  Author: Mikiel Agutu (except where otherwise explicitly stated)

import React from 'react';

class Center extends React.Component{
    render(){
        return (
            <div style={{display: 'flex', justifyContent: 'center'}}>
                {this.props.children}
            </div>
        );
    }
}

class CenterAlign extends React.Component{
    render(){
        return (
            <div style={{textAlign: 'center' }}>
                {this.props.children}
            </div>
        );
    }
}

export { Center, CenterAlign };