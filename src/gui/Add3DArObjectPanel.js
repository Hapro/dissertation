//  Author: Mikiel Agutu (except where otherwise explicitly stated)

import React from 'react';
import { Button, ControlLabel, FormControl, ListGroup, ListGroupItem, Panel } from 'react-bootstrap';
import { connect } from 'react-redux';
import ActionCreators from '../actionCreators/ActionCreators';
import { StringInput } from './Inputs';
import { ARObjectType } from '../lib/AREngine/ARObjects';
import { CompactPicker } from 'react-color';

const ARObjectSelection = [
    {
        name: 'human',
        type: ARObjectType.AR_OBJECT_3D_ANIMATED
    },
    {
        name: 'fighter',
        type: ARObjectType.AR_OBJECT_3D_ANIMATED
    },
    {
        name: 'mage',
        type: ARObjectType.AR_OBJECT_3D_ANIMATED
    },
    {
        name: 'dog',
        type: ARObjectType.AR_OBJECT_3D_ANIMATED
    },
    {
        name: 'dragon',
        type: ARObjectType.AR_OBJECT_3D_ANIMATED
    },
    {
        name: 'checkers - counter',
        type: ARObjectType.AR_OBJECT_3D
    },
    {
        name: 'cross',
        type: ARObjectType.AR_OBJECT_3D
    },
    {
        name: 'chess - knight',
        type: ARObjectType.AR_OBJECT_3D
    },
    {
        name: 'chess - king',
        type: ARObjectType.AR_OBJECT_3D
    },
    {
        name: 'chess - queen',
        type: ARObjectType.AR_OBJECT_3D
    },
    {
        name: 'chess - pawn',
        type: ARObjectType.AR_OBJECT_3D
    },
    {
        name: 'chess - rook',
        type: ARObjectType.AR_OBJECT_3D
    },
    {
        name: 'chess - bishop',
        type: ARObjectType.AR_OBJECT_3D
    },
];


//Component for adding 3D AR Objects
class Add3dArObjectPanel extends React.Component{
    constructor(props) {
        super(props);

        //http://stackoverflow.com/questions/29577977/unable-to-access-react-instance-this-inside-event-handler
        //Bind 'this' keyword to this class
        this.btnAddOnClick = this.btnAddOnClick.bind(this);
        this.onShapeSelectionChange = this.onShapeSelectionChange.bind(this); 
        this.colourOnChange = this.colourOnChange.bind(this);
        this.setModelToAdd = this.setModelToAdd.bind(this);
        this.state = {modelToAdd: { filename: 'human', type: ARObjectType.AR_OBJECT_3D_ANIMATED, colour: '#FFFFFF' }};
    }

    onShapeSelectionChange(event){
        const filename = event.target.value;
        for(let i = 0; i < ARObjectSelection.length; i++){
            if (ARObjectSelection[i].name === filename){
                this.setModelToAdd(ARObjectSelection[i]);
            }
        }
    }

    setModelToAdd(model){
        this.setState({modelToAdd: {filename: model.name, type: model.type }});
    }

    btnAddOnClick(event){
        const { ar3DScene, syncViewWithArEngine, addModel } = this.props;
        const { modelToAdd } = this.state;
        
        addModel(modelToAdd);
    }

    colourOnChange(color){
        this.setState({modelToAdd: {...this.state.modelToAdd, colour: color.hex}});
    }

    render(){
        const { colour, modelToAdd }= this.state;

        const arObjectsOptions = () => {
            const rows = [];
            for(let i = 0; i < ARObjectSelection.length; i++){
                rows.push(<option key={i}>{ARObjectSelection[i].name}</option>);
            }
            return rows;
        }
        
        return(
            <ListGroup>
            <ListGroupItem>
                <ControlLabel>Select 3D Model</ControlLabel>
                <FormControl componentClass="select" placeholder='...' onChange={this.onShapeSelectionChange}>
                    {arObjectsOptions()};
                </FormControl>
            </ListGroupItem>
            <ListGroupItem>
            <strong>Model Colour</strong>
            <CompactPicker
                onChangeComplete={ this.colourOnChange }
                color={ modelToAdd.colour }
            />
            </ListGroupItem>
            <ListGroupItem>
            <ControlLabel>Confirm</ControlLabel><br/>
            <Button onClick={this.btnAddOnClick}>Add</Button>
            </ListGroupItem>
            </ListGroup>
        );
    }
}

const mapStateToProps = (state) => {
  return {
      ar3DScene: state.ar3DScene
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        syncViewWithArEngine: () => { dispatch(ActionCreators.syncViewWithArEngine()); },
        addModel: (model) => { dispatch(ActionCreators.addModel(model)); }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Add3dArObjectPanel);