//  Author: Mikiel Agutu (except where otherwise explicitly stated)

import React from 'react';
import ReactDOM from 'react-dom';
import WebcamCanvas from './WebcamCanvas';
import Ar3dSceneCanvas from './ar3DSceneCanvas';

//Combines 3D imagery, webcam filled canvas etc
//together to form an augmented display
class ArDisplay extends React.Component{
    readyToRender() {
        const { ar3DScene, webcamCapture } = this.props;
        return ar3DScene && webcamCapture; 
    }

    render(){
        const { ar3DScene, webcamCapture } = this.props;

        if(this.readyToRender()){
            return(
                <div>
                    <WebcamCanvas style={{position: 'absolute'}} webcamCapture={webcamCapture}/>
                    <Ar3dSceneCanvas style={{position: 'relative'}} ar3DScene={ar3DScene}/>
                </div>
            );
        }

        return(<p>Loading AR view</p>);
    }
}

export default ArDisplay;