//  Author: Mikiel Agutu (except where otherwise explicitly stated)

import React from 'react';
import { FormGroup, ControlLabel, FormControl } from 'react-bootstrap';

//Static string in the context of a form i.e. text, number, etc
const StaticString = ({title, value}) => {
    return(
        <FormGroup>
            <ControlLabel>{title}</ControlLabel>
                <FormControl.Static>
                    {value}
                </FormControl.Static>
        </FormGroup>
    );
}

//Input any string, i.e. text, number, etc
const StringInput = ({id, label, type, placeholder, defaultValue, onBlur=()=>{}, error='', onChange=(e)=>{}}) => {
    return(
        <FormGroup controlId={id}>
            <ControlLabel>{label}</ControlLabel>
            <FormControl type={type} defaultValue={defaultValue} placeholder={placeholder} onBlur={onBlur} onChange={onChange}/>
            <p style={{color:'red'}}>{error}</p>
        </FormGroup>
    );
}


//Input any colour,
const ColourInput = ({id, label, onBlur=()=>{}, error='', onChange=(e)=>{}}) => {
    return(
        <FormGroup controlId={id}>
            <ControlLabel>{label}</ControlLabel>
            <input className="jscolor" onBlur={onBlur} onChange={onChange}/>
            <p style={{color:'red'}}>{error}</p>
        </FormGroup>
    );
}

//Drop down selection box
const DropDownInput = ({id, label, placeholder, options, onChange, onClick, error=''}) => {
    return(
        <FormGroup controlId={id}>
            <ControlLabel>{label}</ControlLabel>
            <FormControl componentClass='select' placeholder={placeholder} onChange={onChange} onClick={onClick}>
            { options.map((o, i) => {
                return(<option key={i}>{o}</option>)
            })}
            </FormControl>
            <p style={{color:'red'}}>{error}</p>
        </FormGroup>
    );
}

export { DropDownInput, StringInput, StaticString };