//  Author: Mikiel Agutu (except where otherwise explicitly stated)

import React from 'react';
import { Panel, Button, Well, Collapse, Glyphicon, Table } from 'react-bootstrap';
import { connect } from 'react-redux';
import { ARObjectType } from '../lib/ArEngine/ArObjects';
import { DropDownInput, StringInput, StaticString } from './Inputs';
import { AppStates } from '../InitialState';
import InfoButton from './InfoButton';
import { GithubPicker } from 'react-color';

class ArObjectManager extends React.Component{
    constructor(props) {
        super(props);
        this.toggleManagerForMarkerId = this.toggleManagerForMarkerId.bind(this);
        this.getIsManagerForMarkerIdOpen = this.getIsManagerForMarkerIdOpen.bind(this);
        this.state = {opens:[]};
    }

    readyToRender() {
        const { arEngine, appState } = this.props;
        return arEngine !== undefined && arEngine !== null && appState == AppStates.MAIN_STATE;
    }

    getIsManagerForMarkerIdOpen(markerId){
        const { opens } = this.state;

        if(opens[markerId] === undefined) opens[markerId] = false;

        return opens[markerId];
    }

    toggleManagerForMarkerId(markerId){
        const { opens } = this.state;
        opens[markerId] = !opens[markerId];
        this.setState({opens: opens});
    }

    renderSelectedArObjectStuff(arObject){
        if(arObject !== null && arObject !== undefined){
            switch(arObject.Type()){
                case ARObjectType.AR_OBJECT_3D:
                    return (
                        <div>
                            <h3>Selected Object: {arObject.Name()}</h3>
                            <Ar3dObjectManager arObject3d={arObject}/>
                        </div>
                    );

                case ARObjectType.AR_OBJECT_3D_ANIMATED:
                    return (
                        <div>
                            <h3>Selected Object: {arObject.Name()}</h3>
                            <ArAnimated3dObjectManager arAnimatedObject3d={arObject}/>
                        </div>
                    );

                case ARObjectType.AR_OBJECT_3D_IMAGE:
                    return (
                        <h3>Selected Object: {arObject.Name()}</h3>
                    );
            };
    }
    }

    render(){
        if(!this.readyToRender()) return (<p></p>);

        const { arEngine, arObject } = this.props;
        const arObjectCollection = arEngine.ArObjectCollection();
        const infoText = "Click the name of the object to get it's marker. " + 
        "Print this off and place it infront of your camera to display your AR image!";

        return(
            <div>
            <Panel>
                {this.renderSelectedArObjectStuff(arObject)}
            </Panel>
                <h3>Object table <InfoButton text={infoText}/></h3>
                <Table striped bordered condensed hover>
                <thead>
                    <tr>
                        <th>Object name</th>
                        <th>Object settings</th>
                    </tr>
                </thead>
                {
                arObjectCollection.getAll().map((obj, i) =>{
                    const filename = 'res/markers/' + i + '.png';

                    switch(obj.Type()){
                        case ARObjectType.AR_OBJECT_3D:
                            return (
                                <tbody key={i}>
                                <tr>
                                    <td><a href={filename}>{obj.Name()}</a></td>
                                    <td>
                                        <Button onClick={() => {this.toggleManagerForMarkerId(i)}}>
                                            { this.getIsManagerForMarkerIdOpen(i) ? 'Close' : 'View' }
                                        </Button>
                                        <Collapse in = {this.getIsManagerForMarkerIdOpen(i)}>
                                        <Well>
                                            <Ar3dObjectManager arObject3d={obj}/>
                                        </Well>
                                        </Collapse>
                                    </td>
                                </tr>
                                </tbody>
                            );

                        case ARObjectType.AR_OBJECT_3D_ANIMATED:
                            return (
                                <tbody key={i}>
                                <tr>
                                    <td><a href={filename}>{obj.Name()}</a></td>
                                    <td>
                                    <Button onClick={() => {this.toggleManagerForMarkerId(i)}}>
                                        { this.getIsManagerForMarkerIdOpen(i) ? 'Close' : 'View' }
                                    </Button>
                                    <Collapse in = {this.getIsManagerForMarkerIdOpen(i)}>
                                    <Well>
                                        <ArAnimated3dObjectManager arAnimatedObject3d={obj}/>
                                    </Well>
                                    </Collapse>
                                    </td>
                                </tr>
                                </tbody>
                            );

                        case ARObjectType.AR_OBJECT_3D_IMAGE:
                            return (
                                <tbody key={i}>
                                <tr>
                                    <td><a href={filename}>{obj.Name()}</a></td>
                                    <td/>
                                </tr>
                                </tbody>
                            );
                        }
                    })
                }
                </Table>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
  return {
      arEngine: state.arEngine,
      arEngineSync: state.arEngineSync,
      appState: state.appState,
      arObject: state.arObject
    };
}

export default connect(mapStateToProps)(ArObjectManager);

//Principle: React state is initialized with data in AR object
//Update the AR object after state update is confirmed
class ArAnimated3dObjectManager extends React.Component{
    constructor(props) {
        super(props);
        const { arAnimatedObject3d } = this.props;
        const animations = [];
        
        this.animationOnBlur = this.animationOnBlur.bind(this);
        this.componentDidUpdate = this.componentDidUpdate.bind(this);
        this.colourOnBlur = this.colourOnBlur.bind(this);

        arAnimatedObject3d.Animations().map((anim, i) => {
            animations[i] = anim.name;
        });

        this.state = {
            scale: arAnimatedObject3d.Scale(),
            name: arAnimatedObject3d.Name(),
            animations: animations,
            colour: '#' + (arAnimatedObject3d.Colour() ? arAnimatedObject3d.Colour().getHexString() : '000000')
        };
    }

    componentDidUpdate(){
        const { colour } = this.state;
        const { arAnimatedObject3d } = this.props;
        arAnimatedObject3d.setColour(colour);
    }

    animationOnBlur(e){
        const animation = e.target.value;
        const { arAnimatedObject3d } = this.props;
        arAnimatedObject3d.playNewAnimation(animation);
    }

    colourOnBlur(e){
        const colour = e.target.value;
        this.setState({colour: colour});
    }

    render(){
        const { animations, scale, name, colour} = this.state;

        return(
            <div>
                <DropDownInput 
                    id = '0'
                    label='Select animation'
                    placeholder='...'
                    options={animations}
                    onChange={this.animationOnBlur}
                    onClick={this.animationOnBlur}
                />
            </div>
        );
    }
}

class Ar3dObjectManager extends React.Component{
    constructor(props) {
        super(props);
        const { arObject3d } = this.props;
        this.colourOnBlur = this.colourOnBlur.bind(this);
        this.componentDidUpdate = this.componentDidUpdate.bind(this);
        this.state = {
            colour: '#' + (arObject3d.Colour() ? arObject3d.Colour().getHexString() : '000000'),
            name: arObject3d.Name()
        };
    }

    componentDidUpdate(){
        const { colour, rotationOffset } = this.state;
        const { arObject3d } = this.props;
        arObject3d.setColour(colour);
    }

    colourOnBlur(e){
        const colour = e.target.value;
        this.setState({colour: colour});
    }

    render(){
        const { colour, rotationOffset, name } = this.state;

        return(
            <div>
                No settings for this object.
            </div>
        );
    }
}