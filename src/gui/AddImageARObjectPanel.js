//  Author: Mikiel Agutu (except where otherwise explicitly stated)

import React from 'react';
import { Button, ControlLabel, FormControl, ListGroup, ListGroupItem, Panel } from 'react-bootstrap';
import { connect } from 'react-redux';
import ActionCreators from '../actionCreators/ActionCreators';
import { StringInput } from './Inputs';
import { ARObjectType } from '../lib/AREngine/ARObjects';

function encodeImageFileAsURL(success, fail) {
    try{
        const filesSelected = document.getElementById("fileInput").files;
        const file = filesSelected[0];
        const reader  = new FileReader();
        reader.onloadend = function () {
            success(reader.result, file.name);
        }
        reader.readAsDataURL(file);
    }
    catch(error){
        fail(error);
    }
}

//Component for adding 3D AR Objects
class AddImageARObjectPanel extends React.Component{
    constructor(props) {
        super(props);

        //http://stackoverflow.com/questions/29577977/unable-to-access-react-instance-this-inside-event-handler
        //Bind 'this' keyword to this class
        this.btnAddOnClick = this.btnAddOnClick.bind(this);

        this.state = {modelToAdd: null, error: null };
    }


    setModelToAdd(model){
        this.setState({error: null, modelToAdd: model });
    }

    btnAddOnClick(event){
        const { ar3DScene, syncViewWithArEngine, addModel } = this.props;
        const { modelToAdd } = this.state;
        
        if(modelToAdd != null){
            addModel(modelToAdd);
        }
    }

    render(){
        const { error, modelToAdd }= this.state;
        const cb = (base64Img, filename="img") => {
            this.setModelToAdd({filename: filename, data: base64Img, type: ARObjectType.AR_OBJECT_3D_IMAGE });
        };

        const failCb = (error) => {
            this.setState({error: error});
        };

        return(
            <ListGroup>
            <ListGroupItem>
            <ControlLabel>Upload image</ControlLabel>
            { error !== null ? error : null }
                <input accept="image/*" id="fileInput" type="file" onChange={() => {encodeImageFileAsURL(cb, failCb)}} />
            <ControlLabel>Confirm</ControlLabel><br/>
            <Button onClick={this.btnAddOnClick}>Add</Button>
            </ListGroupItem>
            </ListGroup>
        );
    }
}

const mapStateToProps = (state) => {
  return {
      ar3DScene: state.ar3DScene
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        syncViewWithArEngine: () => { dispatch(ActionCreators.syncViewWithArEngine()); },
        addModel: (model) => { dispatch(ActionCreators.addModel(model)); }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddImageARObjectPanel);