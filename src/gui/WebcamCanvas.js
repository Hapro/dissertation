//  Author: Mikiel Agutu (except where otherwise explicitly stated)

import React from 'react';
import ReactDOM from 'react-dom';

//Component for displaying the canvas in the WebcamCapture object.
class WebcamCanvas extends React.Component{
    componentDidMount(){
        const { webcamCapture } = this.props;
        ReactDOM.findDOMNode(this.refs.webcamWrapper).appendChild(webcamCapture.Canvas());
    }

    render(){
        const { webcamCapture, style } = this.props;

        if(webcamCapture.Errors().length > 0){
            const errors = webcamCapture.Errors();
            return <ul>{errors.map(err =>{return <li>{err}</li>})}</ul>;
        }

        return(
            <div style={style} ref='webcamWrapper'></div>
        )
    }
}

export default WebcamCanvas;