//  Author: Mikiel Agutu (except where otherwise explicitly stated)

import React from 'react';
import { Grid, Row, Col, Button, Navbar } from 'react-bootstrap';
import { StringInput } from './Inputs';
import { connect } from 'react-redux';
import ActionCreators from '../actionCreators/ActionCreators';
import { Center, CenterAlign } from './Center';
import InfoButton from './InfoButton';

//UI for choosing to join or start an AR game
class JoinStartGame extends React.Component{
    constructor(props){
        super(props);
        this.state = {password:''};
    }

    render(){
        const { joinRefused, startGame, joinGame, ar3DScene } = this.props;
        const error = joinRefused ? 'Could not join game. Password may be invalid.' : '';
        const infoTextStart = 'Create a server which others can join to experience AR in real-time!';
        const infoTextJoin = 'Ask your friend for their AR server password to enjoy AR in real-time!';

        return(
            <CenterAlign>
                <h3>Start your own AR game session <InfoButton text={infoTextStart}/></h3>
                <Button onClick={() => { startGame(); }}>Start game</Button>
                <h3>Or join one in progress <InfoButton text={infoTextJoin}/></h3>
                <StringInput
                id='0'
                type='text'
                placeholder='Game password'
                defaultValue=''
                error={error}
                onChange={(e) => {this.setState({password: e.target.value});}}/>
                <Button onClick={() => {joinGame(ar3DScene, this.state.password)}}>Join game</Button>
            </CenterAlign>
        );
    }
}

const mapStateToProps = (state) => {
  return {
      ar3DScene: state.ar3DScene
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        startGame: () => { dispatch(ActionCreators.startGameServer()); },
        joinGame: (ar3DScene, password) => { dispatch(ActionCreators.joinGame(ar3DScene, password)); }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(JoinStartGame);