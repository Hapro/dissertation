//  Author: Mikiel Agutu (except where otherwise explicitly stated)

import React from 'react';
import { Button, ControlLabel, FormControl, ListGroup, ListGroupItem, Panel } from 'react-bootstrap';
import { connect } from 'react-redux';
import ActionCreators from '../actionCreators/ActionCreators';
import { StringInput } from './Inputs';
import { ARObjectType } from '../lib/AREngine/ARObjects';

//Component for adding 3D AR Objects
class AddCustomArObjectPanel extends React.Component{
    constructor(props) {
        super(props);

        //http://stackoverflow.com/questions/29577977/unable-to-access-react-instance-this-inside-event-handler
        //Bind 'this' keyword to this class
        this.btnAddOnClick = this.btnAddOnClick.bind(this);
        this.setModelToAdd = this.setModelToAdd.bind(this);
        this.txtNameOnChange = this.txtNameOnChange.bind(this);
        this.chkObjectAnimatedOnChange = this.chkObjectAnimatedOnChange.bind(this);

        this.state = {error: null, animated: false, name: "", base64JsonData: "" };
    }

    btnAddOnClick(event){
        const { addModel } = this.props;
        const { animated, name, base64JsonData } = this.state;
        let type;

        if(animated){
            type = ARObjectType.AR_OBJECT_3D_ANIMATED;
        }
        else{
            type = ARObjectType.AR_OBJECT_3D;
        }
        
        if(base64JsonData != null){
            addModel({filename: name, type: type, base64JsonData: base64JsonData, custom:true });
        }
    }

    setModelToAdd(success, fail){
        try{
            const filesSelected = document.getElementById("fileInputCustomObject").files;
            const file = filesSelected[0];
            const reader  = new FileReader();
            reader.onloadend = function () {
                let res = reader.result.replace('data:;base64,', '');
                success(res, file.name);
            }
            reader.readAsDataURL(file);
        }
        catch(error){
            fail(error);
        }

    }

    chkObjectAnimatedOnChange(){
        const { animated } = this.state;
        this.setState({animated: !animated })
    }

    txtNameOnChange(event){
        this.setState({name: event.target.value})
    }

    render(){
        const { error, name, animated }= this.state;
        const cb = (base64JsonData, filename="") => {
            this.setState({error: null, animated: animated, name: name, base64JsonData: base64JsonData });
        };

        const failCb = (error) => {
            this.setState({error: error});
        };

        return(
            <ListGroup>
            <ListGroupItem>
            <ControlLabel>Upload custom model</ControlLabel><br/>
            <strong>Advanced functionality. Corrupt or invalid files can cause errors.</strong>
            <p>Upload your own Three.js compatible JSON file for display in the game.</p>
            { error !== null ? error : null }
                <input accept="text/*" id="fileInputCustomObject" type="file" onChange={() => {this.setModelToAdd(cb, failCb)}} />
            <ControlLabel>Animated? </ControlLabel>
            <input type='checkbox' onChange={() => {this.chkObjectAnimatedOnChange()}}/>
            <StringInput
                id='1'
                label='Name'
                type='text'
                placeholder=''
                defaultValue={""}
                onBlur={this.txtNameOnChange}/>
            <ControlLabel>Confirm</ControlLabel><br/>
            <Button onClick={this.btnAddOnClick}>Add</Button>
            </ListGroupItem>
            </ListGroup>
        );
    }
}

const mapStateToProps = (state) => {
  return {
      ar3DScene: state.ar3DScene
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        syncViewWithArEngine: () => { dispatch(ActionCreators.syncViewWithArEngine()); },
        addModel: (model) => { dispatch(ActionCreators.addModel(model)); }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddCustomArObjectPanel);