//  Author: Mikiel Agutu (except where otherwise explicitly stated)

import React from 'react';
import { Grid, Row, Col, Button, Navbar, Panel } from 'react-bootstrap';
import AddArObjectPanel from './AddArObjectPanel';
import ArObjectManager from './ArObjectManager';
import ArDisplay from './ArDisplay';
import JoinStartGame from './JoinStartGame';
import { connect } from 'react-redux';
import InitialState from '../InitialState';
import { AppStates } from '../InitialState';
import SelectVideoSource from './SelectVideoSource';
import ActionCreators from '../actionCreators/ActionCreators';
import ViewMarkers from './ViewMarkers';
import { Center } from './Center';
import InfoButton from './InfoButton';

const SelectWebcam = (webcamCapture) => {
    return(
        <Row className='show-grid'>
            <Col md={12} id='arDisplay'>
                <SelectVideoSource webcamCapture={webcamCapture}/>
            </Col>
        </Row>
    );
};

const SetupGame = (models, ar3DScene, gameSetUp) => {
    return(
        <Row className='show-grid'>
            <Col md={7} id='arDisplay'>
                <AddArObjectPanel/>
                <Button onClick={() => gameSetUp(models, ar3DScene)}>Finished setup</Button>
            </Col>
            <div>
            <Col md={5}>
                <ViewMarkers/>
            </Col>
        </div>
        </Row>
    );
};

const MainState = (gameServerPassword, webcamCapture, ar3DScene) => {
    return (
        <Row className='show-grid'>
            <Col md={6} id='arDisplay'>
                <h4>Server password: {gameServerPassword} 
                 <InfoButton text={"Share this with your friends so they can this AR game."}/>
                </h4>
                <strong>Frozen? Refresh the app and re-join this game with the password to refresh the display.</strong>
                <Center><ArDisplay webcamCapture={webcamCapture} ar3DScene={ar3DScene}/></Center>
            </Col>
            <Col md={2}/>
            <Col md={4}>
                <ArObjectManager/>
            </Col>
        </Row>
    );
};

const JoinOrStartGame = () => {
    return <JoinStartGame joinRefused={false}/>;
}

const JoinGameRefused= () => {
    return <JoinStartGame joinRefused={true}/>
}

const errorBox = (error, clearError) =>{
    if(error === null || error === '')
        return null;

        return (
            <Col md={12}>
                <Panel>
                <strong>
                    Error! { error } 
                </strong>
                <Button onClick={() => {clearError()}}>Clear</Button></Panel>
            </Col>
        );
}

class App extends React.Component{
    renderBasedOnAppState(){
        const { appState, webcamCapture, ar3DScene, gameSetUp, models, gameServerPassword } = this.props;

        switch(appState){
            case AppStates.SELECT_WEBCAM:
                return SelectWebcam(webcamCapture);

            case AppStates.SETUP_GAME:
                return SetupGame(models, ar3DScene, gameSetUp);

            case AppStates.MAIN_STATE:
                return MainState(gameServerPassword, webcamCapture, ar3DScene);

            case AppStates.JOIN_OR_START_GAME:
                return JoinOrStartGame();

            case AppStates.JOIN_GAME_REFUSED:
                return JoinGameRefused();
        }

        return <div/>;
    }

    render(){
        const { webcamCapture, ar3DScene, appState, clearError, error } = this.props;

        return(
            <div id='top'>
            <Navbar>
                <Navbar.Header>
                    <Navbar.Brand>
                        Augmented reality
                    </Navbar.Brand>
                </Navbar.Header>
            </Navbar>
            <Grid>
                { errorBox(error, clearError )}
                {this.renderBasedOnAppState()}
                <Row className='show-grid'>
                    <Col md={12}></Col>
                </Row>
            </Grid>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
  return {
      webcamCapture: state.webcamCapture,
      ar3DScene: state.ar3DScene,
      appState: state.appState,
      models: state.models,
      gameServerPassword: state.gameServerPassword,
      error: state.error
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        gameSetUp: (models, ar3DScene) => { dispatch(ActionCreators.gameSetUp(models, ar3DScene)); },
        clearError: () => { dispatch(ActionCreators.error(''))}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);