//  Author: Mikiel Agutu (except where otherwise explicitly stated)

import React from 'react';
import { Button } from 'react-bootstrap';
import Add3dArObjectPanel from './Add3dArObjectPanel';
import ActionCreators from '../actionCreators/ActionCreators';
import { connect } from 'react-redux';
import { Center, CenterAlign } from './Center';
import InfoButton from './InfoButton';

//Component for selecting video source to use
class SelectVideoSource extends React.Component{
    readyToRender() {
        const { webcamCapture } = this.props;
        return webcamCapture && webcamCapture.GetVideoSourcesLoaded();
    }

    sourceSelectOnClick(deviceId){
        const { webcamCapture, videoSourceSelected } = this.props;
        webcamCapture.createVideoStream(deviceId);
        videoSourceSelected();
    }

    render(){
        const { webcamCapture } = this.props;
        const infoText = 'If your camera is not present you may need to update your browser.';

        if(this.readyToRender()){
            const videoSources = webcamCapture.GetVideoSources();

            if(webcamCapture.Errors().length > 0){
                const errors = webcamCapture.Errors();
                return <ul>{errors.map(err =>{return <li>{err}</li>})}</ul>;
            }
            
            return(
                <div>
                    <CenterAlign><h3>Camera select  <InfoButton text={infoText}/></h3>
                    <p>This is a list of all the camera devices that have been detected. Select the camera you would like to use.</p>
                    {videoSources.map(s =>{ return <Button key={s.label} onClick={()=> this.sourceSelectOnClick(s.id)}>{s.label}</Button>})}
                    <br/>
                    </CenterAlign>
                </div>
            )
        }
        else{
            return (
                <div>
                    <h2>Loading...</h2>
                    <p>If you are able to read this then the following issues may be present:</p>
                    <ul>
                        <li>You have not granted permission for this app to access your webcam. Fix this by refreshing the page and granting webcam access permissions when prompted.</li>
                        <li>Your browser is out of date or incompatible with this application. Fix this by updating your browser, or using a different browser entierly. Google Chrome is the recommended browser for this app.</li>
                    </ul>
                </div>
            );
        }
    }
}

const mapStateToProps = (state) => {
  return {
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        videoSourceSelected: () => { dispatch(ActionCreators.videoSourceSelected()); }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SelectVideoSource);
