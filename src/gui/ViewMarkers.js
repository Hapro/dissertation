//  Author: Mikiel Agutu (except where otherwise explicitly stated)

import React from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { Table, Button } from 'react-bootstrap';
import InfoButton from './InfoButton';
import ActionCreators from '../actionCreators/ActionCreators';

//Combines 3D imagery, webcam filled canvas etc
//together to form an augmented display
class ViewMarkers extends React.Component{
    render(){
        const { models, removeModel } = this.props;
        const infoText = "Click on the name of each object to get the marker required to display it." +
        "These markers can then be printed out, and used to view the 3D object in the display.";

        return(
            <div>
                <Table striped bordered condensed hover>
                <thead>
                    <tr>
                        <th>Object name</th>
                        <th>Object colour</th>
                        <th>Options</th>
                    </tr>
                </thead>
                    {models.map((m, i) =>{
                        const filename = 'res/markers/' + i + '.png';
                        return(
                            <tbody key={i}>
                            <tr>
                                <td><a href={filename}>{m.filename}</a></td>
                                <td><div style={{width: 40, height: 40, backgroundColor: m.colour}}/></td>
                                <td><Button onClick={() => {removeModel(m.id)}}>Remove</Button></td>
                            </tr>
                            </tbody>
                        );
                    })}
                </Table>
            </div>
        ); 
    }
}

const mapStateToProps = (state) =>{
    return{
        models: state.models
    }
}

const mapDispatchToProps = (dispatch) =>{
    return {
        removeModel: (id) => { dispatch(ActionCreators.removeModel(id)); },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewMarkers);