//  Author: Mikiel Agutu (except where otherwise explicitly stated)

//Class for accessing webcam
const WebcamCapture = () => {
    let video;
    let canvas;
    let context;
    let errors = [];
    let videoSources;
    let videoSourcesLoaded = false;

    return {
        Canvas: () => { return canvas; },
        Video: () => { return video; },
        CanvasContext: () => { return context; },
        Errors: () => { return errors; },
        GetVideoSources: () => { return videoSources; },
        GetVideoSourcesLoaded: () => { return videoSourcesLoaded; },

        //Get user permissions to access webcam through browser
        getPermissions: (grantedCallback, errorCallback) =>{

            //Use get use rmedia api
            navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

            //If no API available throw error
            if (!navigator.getUserMedia){
                errorCallback();
            }
            else{
                //Attempt to get video permissions 
                navigator.getUserMedia({video: true }, stream => { grantedCallback(); }, error => { errorCallback() });          
            }

            return errors;
        },

        //Detect video sources connected to browser
        detectVideoSources: (callback = () => {}) => {
            videoSourcesLoaded = false;
            videoSources = [];
            let i = 0;

            navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

            if (navigator.getUserMedia){
                //Enumerate devices and return list of these
                navigator.getUserMedia({video: true }, stream => {
                    navigator.mediaDevices.enumerateDevices()
                        .then(devices => {
                            devices.forEach(device => {
                                if(device.kind === 'videoinput'){
                                    videoSources[i++] = { id: device.deviceId, label: device.label};
                                }
                            }
                        );
                        videoSourcesLoaded = true;
                        callback();
                        }
                    );
                },
                error => {});     
            }
        },

        //Create a new video stream and display it in the canvas
        createVideoStream: (videoSourceId = null) => {      
            errors = [];

            navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

            if (!navigator.getUserMedia){
                errors.push("Browser does not support getUserMedia");
            }
            else{
                //Success callback
                const getUserMediaSuccess = stream => {
                    if (window.URL){
                        //Set video src to the camera stream
                        video.src = window.URL.createObjectURL(stream);
                    } 
                    else{
                        if (video.mozSrcObject !== undefined){
                            video.mozSrcObject = stream;
                        }
                        else{
                            video.src = stream;
                        }
                    }   
                };

                const constraints = videoSourceId !== null ? {
                    video: {
                        optional: [{
                            sourceId: videoSourceId
                        }]
                    }
                } : { video: true };
                
                const initializeVideo = () => navigator.getUserMedia(constraints, getUserMediaSuccess, error => errors.push(error));
                
                initializeVideo();
            }

            return errors;
        },

        //Creates the canvas elements
        createWebcamCanvas: (width, height, displayCanvasWidth, displayCanvasHeight) => {
            video = document.createElement("video");
            
            canvas = document.createElement("canvas");
            context = canvas.getContext("2d");

            canvas.width = width;
            canvas.height = height;
        },

        //Update the canvas to display video feed from the webcam
        updateCanvasWithWebcamData: () => {
            context.drawImage(video, 0, 0);
            canvas.changed = true;
        },

        //Returns true if the video element has enough data from the webcam
        webcamReadyToCapture: () => {
            return video.readyState === video.HAVE_ENOUGH_DATA;
        }
    };
};

export default WebcamCapture;