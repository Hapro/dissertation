//  Author: Mikiel Agutu (except where otherwise explicitly stated)

import * as THREE from '../three';

//Data structure which maintains a Three.js scene
const ThreeScene = (scene, camera, lights, objects) => {
    return {
        Objects: () => { return objects; },
        Camera: () => { return camera; },
        Scene: () => { return scene; },
        
        addObjectToThreeScene: (object) => {
            objects.push(object);
            scene.add(object);
        },

        removeObjectFromThreeScene: (object) => {
            scene.remove(object);
        },

        render: (renderer) =>{
            renderer.clear();
            renderer.render(scene, camera);
        }
    }
};

//Class to load textures using Three.js
const TextureLoader = (filepath) => {
    return {
        toFileName: function(name){
            return name + '.jpg';
        },

        loadTexture: (name, callback) => {
            const loader = new THREE.TextureLoader();
            loader.load('/res/textures/' + name + '.jpg', callback);
        }
    };
};

//Class to load models using Three.js
const ModelLoader = (filepath) => {
    return {
        toFileName: (name) => {
            return name + '.json';
        },

        loadModel: (name, callback) => {
            const loader = new THREE.JSONLoader();
            loader.load('/res/models/' + name + '.json', callback);
        },

        parseJsonToModel: (base64JsonData, callback) => {
            const loader = new THREE.JSONLoader();
            const model = loader.parse(JSON.parse(atob(base64JsonData)));

            callback(model.geometry, model.materials);
        }
    };
}

//Class containing helper methods for setting up Three.js scees
const ThreeUtils = () => {
    const textureLoader = TextureLoader("images");
    const modelLoader = ModelLoader("models");

    return {
        //Create a WebGL renderer
        createRenderer: (width, height, clearColour) =>{
            const renderer = new THREE.WebGLRenderer( {alpha: true} );
            renderer.setClearColor(clearColour, 0);
            renderer.setSize(width, height);
            return renderer;
        },

        //Creates ThreeSceneData instance with light and a camera
        createBasicScene: (viewWidth, viewHeight) => {
            const scene = new THREE.Scene();
            const camera = new THREE.PerspectiveCamera(40, viewWidth / viewHeight, 1, 1000);
            const pointLight = new THREE.PointLight(0xFFFFFF);
            const hemishpereLight = new THREE.HemisphereLight( 0xffffbb, 0x080820, 1 );

            pointLight.position.x = 0;
            pointLight.position.y = 0;
            pointLight.position.z = 0;

            scene.add(pointLight);
            scene.add(hemishpereLight);
            scene.add(camera);

            return ThreeScene(scene, camera, [pointLight], []);
        },

        createUVCoords: (leftX = 0, rightX = 1, bottomY = 0, topY = 1) => {
            return[
                new THREE.Vector2(leftX, bottomY),
                new THREE.Vector2(rightX, bottomY),
                new THREE.Vector2(rightX, topY),
                new THREE.Vector2(leftX, topY)
            ];
        },

        //Load a Three.js texture from base64 image data
        loadTextureFromImageData: (imageData, callback) => {
            const textureImage = new Image();
            const texture = new THREE.Texture();
            textureImage.src = imageData;
            textureImage.onload = function() {
                texture.image = textureImage;
                texture.needsUpdate = true;
                callback(texture);
            };
        },

        createMeshLambertMaterial: (color) => {
            return new THREE.MeshLambertMaterial({ color });
        },

        createBoxBufferGeometry: (width, height, depth) => {
            return new THREE.BoxBufferGeometry(width, height, depth);
        },

        createSphereGeometry: (size) => {
            return new THREE.SphereGeometry(size, 16, 16, Math.PI);
        },

        createMeshFaceMaterial: (materials) => {
            return new THREE.MeshFaceMaterial(materials);
        },

        createMeshObject: (geometry, material) => {
            const object = new THREE.Object3D();
            const mesh = new THREE.Mesh(geometry, material);
            object.add(mesh);
            
            return object;
        },

        createCube: (width, height, depth, color = 0xCC0000) => {
            const object = new THREE.Object3D();
            const cubeMaterial = new THREE.MeshLambertMaterial({ color: 0xCC0000 });
            const cubeGeometry = new THREE.BoxBufferGeometry(width, height, depth);
            const mesh = new THREE.Mesh(cubeGeometry, cubeMaterial);

            object.add(mesh);

            return object;
        },

        createSphere: (size, color = 0xCC0000) => {
            const object = new THREE.Object3D();
            const sphereMaterial = new THREE.MeshLambertMaterial({ color: 0xCC0000 });
            const sphereGeometry = new THREE.SphereGeometry(size, 16, 16, Math.PI);
            const mesh = new THREE.Mesh(sphereGeometry, sphereMaterial);

            object.add(mesh);

            return object;
        },

        createTexturedPlane: (texture) =>{
            const object = new THREE.Object3D();
            const planeMaterial = new THREE.MeshLambertMaterial({ map : texture, color: 0xffffff });
            planeMaterial.map.needsUpdate = true;
            const planeGeometry = new THREE.PlaneGeometry(texture.image.width, texture.image.height);
            const mesh = new THREE.Mesh(planeGeometry, planeMaterial);
            object.add(mesh);

            return object;
        },

        parseJsonToModel: (base64JsonData, callback) => {
            modelLoader.parseJsonToModel(base64JsonData, callback);
        },

        loadModel: (name, callback) => {
            modelLoader.loadModel(name, callback);
        }
    }
}

export default ThreeUtils;