//  Author: Mikiel Agutu (except where otherwise explicitly stated)

//Takes makes use of webcamCapture and webcamCaptureData
import AR from "../arcuro/aruco";
import POS from "../arcuro/posit1";
import { ARObject3D, ARObjectCollection, ARObject } from './ARObjects';

//Data structure holding a marker pose
const MarkerPose = (posit, marker, imageWidth, imageHeight) => {
    const corners = marker.corners;

    for (let i = 0; i < corners.length; i++){
        const corner = corners[i];
        corner.x = corner.x - (imageWidth / 2);
        corner.y = (imageHeight / 2) - corner.y;
    }

    const pose = posit.pose(corners);

    return {
        translation: createTranslation(pose.bestTranslation),
        rotation: createRotation(pose.bestRotation),
    }; 
}

const createTranslation = (translation) => { return { x: translation[0], y: translation[1], z: -translation[2] } };

const createRotation = (rotation) => { return { x: -Math.asin(-rotation[1][2]),
                                                y: -Math.atan2(rotation[0][2], rotation[2][2]),
                                                z: Math.atan2(rotation[1][0], rotation[1][1])}}


const AREngine = (modelSize, displayWidth, displayHeight) => {
    //Initialize the various components
    const detector = new AR.Detector();
    const posit = new POS.Posit(modelSize, displayWidth);
    const previouslyDetectedMarkers = [];
    const arObjectCollection = ARObjectCollection();

    return {
        DisplayWidth: () => { return displayWidth; },
        DisplayHeight: () => { return displayHeight; },
        ArObjectCollection: () => { return arObjectCollection; },

        addArObject: (markerId, arObject) => { arObjectCollection.add(markerId, arObject); },

        removeArObject: (markerId) => { arObjectCollection.remove(markerId); },

        detectMarkers: () => {
            //Use the AR library to detect the markers
            const markers = detector.detect();
            const markerDataCollection = [];

            //Add data to the marker collection
            for(let i in markers){
                markerDataCollection.push({
                    pose: MarkerPose(posit, markers[i], displayWidth, displayHeight),
                    id: markers[i].id
                });
            } 
            
            return markerDataCollection;
        },

        run: function(){
            const markerData = this.detectMarkers();

            for(let i in markerData){
                const md = markerData[i];

                //If there is a corresponding ARObject for this marker call onDetect event
                if(arObjectCollection.exists(md.id)){
                    arObjectCollection.get(md.id).onDetect(md.pose);
                }
            }
        }
    }
};


export default AREngine;