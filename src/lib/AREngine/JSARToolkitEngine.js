//  Author: Mikiel Agutu (except where otherwise explicitly stated)

import JSARToolKit from '../JSARToolKit/JSARToolKit';
import { ARObject3D, ARObjectCollection, ARObject } from './ARObjects';
///NOTE: This one cannot cope with 16:9 resoltuions
const MarkerPose = (arMat) => {
    //Convert matrix detected by AR engine to a glMatrix
    //http://glmatrix.net/
    //Adapted from code found on https://www.html5rocks.com/en/tutorials/webgl/jsartoolkit_webrtc/
    const glMatrix = new Float32Array(16);
    glMatrix[0] = arMat.m00;
    glMatrix[1] = -arMat.m10;
    glMatrix[2] = arMat.m20;
    glMatrix[3] = 0;
    glMatrix[4] = arMat.m01;
    glMatrix[5] = -arMat.m11;
    glMatrix[6] = arMat.m21;
    glMatrix[7] = 0;
    glMatrix[8] = -arMat.m02;
    glMatrix[9] = arMat.m12;
    glMatrix[10] = -arMat.m22;
    glMatrix[11] = 0;
    glMatrix[12] = arMat.m03;    //x translation
    glMatrix[13] = -arMat.m13;   //y translation
    glMatrix[14] = arMat.m23;    //z translation
    glMatrix[15] = 1;

    return {
        //translation: createTranslation(glMatrix),
        //rotation: createRotation(glMatrix),
        matrix: glMatrix
    }; 
}

const createTranslation = (glMatrix) => { return { x: glMatrix[12], y: glMatrix[13], z: -glMatrix[14] } };

const createRotation = (glMatrix) => {
    //Algorithm taken from http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/index.htm
    let S = 0;
    const trace = glMatrix[0] + glMatrix[5] + glMatrix[10];
    const out = [0,0,0,0];

    if (trace > 0) { 
        S = Math.sqrt(trace + 1.0) * 2;
        out[3] = 0.25 * S;
        out[0] = (glMatrix[6] - glMatrix[9]) / S;
        out[1] = (glMatrix[8] - glMatrix[2]) / S; 
        out[2] = (glMatrix[1] - glMatrix[4]) / S; 
    } else if ((glMatrix[0] > glMatrix[5])&(glMatrix[0] > glMatrix[10])) { 
        S = Math.sqrt(1.0 + glMatrix[0] - glMatrix[5] - glMatrix[10]) * 2;
        out[3] = (glMatrix[6] - glMatrix[9]) / S;
        out[0] = 0.25 * S;
        out[1] = (glMatrix[1] + glMatrix[4]) / S; 
        out[2] = (glMatrix[8] + glMatrix[2]) / S; 
    } else if (glMatrix[5] > glMatrix[10]) { 
        S = Math.sqrt(1.0 + glMatrix[5] - glMatrix[0] - glMatrix[10]) * 2;
        out[3] = (glMatrix[8] - glMatrix[2]) / S;
        out[0] = (glMatrix[1] + glMatrix[4]) / S; 
        out[1] = 0.25 * S;
        out[2] = (glMatrix[6] + glMatrix[9]) / S; 
    } else { 
        S = Math.sqrt(1.0 + glMatrix[10] - glMatrix[0] - glMatrix[5]) * 2;
        out[3] = (glMatrix[1] - glMatrix[4]) / S;
        out[0] = (glMatrix[8] + glMatrix[2]) / S;
        out[1] = (glMatrix[6] + glMatrix[9]) / S;
        out[2] = 0.25 * S;
    }

    return { x: out[0], y: out[1], z: out[2] };
}


const JSARToolkitEngine = (modelSize, displayWidth, displayHeight, canvas) => {
    const imageReader = new JSARToolKit.NyARRgbRaster_Canvas2D(canvas);
    const param = new JSARToolKit.FLARParam(displayWidth, displayHeight);
    const detector = new JSARToolKit.FLARMultiIdMarkerDetector(param, 80);
    const previouslyDetectedMarkers = [];
    const arObjectCollection = ARObjectCollection();

    detector.setContinueMode(true); 

    return {
        DisplayWidth: () => { return displayWidth; },
        DisplayHeight: () => { return displayHeight; },
        ArObjectCollection: () => { return arObjectCollection; },

        initializeThreeCamera: (threeCamera) => {
            const m = new Float32Array(16);
            param.copyCameraMatrix(m, 10, 10000);
            threeCamera.projectionMatrix.set(
                m[0], m[4], m[8], m[12],
                m[1], m[5], m[9], m[13],
                m[2], m[6], m[10], m[14],
                m[3], m[7], m[11], m[15]);
        },

        addArObject: (markerId, arObject) => { arObjectCollection.add(markerId, arObject); },

        removeArObject: (markerId) => { arObjectCollection.remove(markerId); },

        detectMarkers: () => {
            //second param is threshold
            const detectMarkerLite = detector.detectMarkerLite(imageReader, 128);
            const markerDataCollection = [];

            for (let p = 0; p < detectMarkerLite; p++) {
                const resultMatrix = new JSARToolKit.NyARTransMatResult();
                
                //Get the transform matrix for detected marker
                detector.getTransformMatrix(p, resultMatrix);

                //Get ID of detected marker
                const id = detector.getIdMarkerData(p);

                var currId;
                if (id.packetLength > 4) {
                    currId = -1;
                }else{
                    currId=0;
                    //最大4バイト繋げて１個のint値に変換
                    for (var i = 0; i < id.packetLength; i++ ) {
                    currId = (currId << 8) | id.getPacketData(i);
                    //console.log("id[", i, "]=", id.getPacketData(i));
                    }
                }

                markerDataCollection.push({
                    id: currId,
                    pose: MarkerPose(resultMatrix)
                });
            }

            return markerDataCollection;
        },

        run: function(){
            const markerData = this.detectMarkers();

            arObjectCollection.getAll().map((obj, i) =>{
                 obj.onUpdate();
            });

            for(let i in markerData){
                const md = markerData[i];

                //If there is a corresponding ARObject for this marker call onDetect event
                if(arObjectCollection.exists(md.id)){
                    arObjectCollection.get(md.id).onDetect(md.pose);
                }
            }
        }
    }
};


export default JSARToolkitEngine;