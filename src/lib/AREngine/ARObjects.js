//  Author: Mikiel Agutu (except where otherwise explicitly stated)

const ARObjectType = {
    AR_OBJECT_3D: 'AR_OBJECT_3D',
    AR_OBJECT_3D_ANIMATED: 'AR_OBJECT_3D_ANIMATED',
    AR_OBJECT_3D_IMAGE: 'AR_OBJECT_3D_IMAGE'
};


function toRad(v) {
    return (v * Math.PI) / 180;
}
//Interface; AR object maps a marker ID to an action to perform on marker events
//This is just to define the interface, don't use this unless for mocking
const ARObject = (name) => {
    return {
        Name: () => { return name; },
        Type: () => { return ARObjectType.AR_OBJECT_3D; },
        onDetect: (pose) => {},
        onDelete: () => {},
        onUpdate: () => {},
        onSelect: () => {},
        onUnSelect: () => {}
    }
}

//Data structure for and augmented 3d image
const ARObject3DImage = (name, object, onDelete = () => {}) => {
    const arObject3D = ARObject3D(name, object, onDelete);
    let colour;

    return {
        ...arObject3D,
        Type: () => { return ARObjectType.AR_OBJECT_3D_IMAGE; },
        Colour: () => { return object.children[0].material.color; },
        setColour: (c) => { colour = c; object.children[0].material.color.set(c); },
        onSelect: () => {
            object.children[0].material.color.set(0xFF0000);
        },
        onUnSelect: () => {
            object.children[0].material.color.set(colour);
        }
    }
}

//Data structure for an augmented 3d object
const ARObject3D = (name, object, onDelete = () => {}) => {
    const DRAW_TIMEOUT = 500;
    let oldDate = new Date();
    let colour;
    object.matrixAutoUpdate = false;

    return {
        Name: () => { return name; },
        Object: () => { return object; },
        Type: () => { return ARObjectType.AR_OBJECT_3D },
        onDelete: () => { return onDelete(); },
        Colour: () => { return object.children[0].material.materials[0].color; },
        Scale: () => { return object.scale; },  
        setColour: (c) => { colour = c; object.children[0].material.materials[0].color.set(c); },
        onUpdate: () => {
            const currentDate = new Date();
            object.visible = (currentDate.getTime() - oldDate.getTime()) < DRAW_TIMEOUT;
        },
        onSelect: () => {
            object.children[0].material.materials[0].color.set(0xFF0000);
        },
        onUnSelect: () => {
            object.children[0].material.materials[0].color.set(colour);
        },

        onDetect: (pose) => {
            const m = pose.matrix;

            object.matrix.set(
                m[0], m[4], m[8], m[12],
                m[1], m[5], m[9], m[13],
                m[2], m[6], m[10], m[14],
                m[3], m[7], m[11], m[15]);
            
            object.matrix.matrixWorldNeedsUpdate = true;
            object.visible = true;

            oldDate = new Date();
        }
    }
}

//Data structure for animated 3d object
const ARObject3DAnimated = (animations, clock, mixer, name, object, onDelete = () => {}) => {
    const arObject3D = ARObject3D(name, object, onDelete);
    let colour;
    const getAnimationByName = (name) => {
        let animationToReturn;

        animations.map(anim => {
            if(anim.name === name) animationToReturn = anim;
        });

        return animationToReturn;
    };

    getAnimationByName('Pose').play();

    let currentAnimationName = 'Pose';

    return {
        ...arObject3D,
        Type: () => { return ARObjectType.AR_OBJECT_3D_ANIMATED },
        Animations: () => { return animations; },
        PauseCurrentAnimation: () => { return getAnimationByName(currentAnimationName).stop(); },
        PlayCurrentAnimation: () => { return getAnimationByName(currentAnimationName).play(); },
        Colour: () => { return object.material.materials[0].color; },
        setColour: (c) => { colour = c; object.material.materials[0].color.set(c); },
        playNewAnimation: (name) => {
            if(currentAnimationName != null) getAnimationByName(currentAnimationName).stop();
            currentAnimationName = name;
            getAnimationByName(currentAnimationName).play();
         },
         onUpdate: () => {
            mixer.update(clock.delta);
            arObject3D.onUpdate();
         },
        onDetect: (pose) => {
            arObject3D.onDetect(pose);
        },
        onSelect: () => {
            object.material.materials[0].color.set(0xFF0000);
        },
        onUnSelect: () => {
            object.material.materials[0].color.set(colour);
        }
    }
}

const ARObjectCollection = () => {
    const arr = [];

    return {
        clear: () => { arr.length = 0; },

        getAll: () => { return arr; },

        get: (markerId) => { return arr[markerId]; },

        exists: (markerId) => { return arr[markerId] !== undefined; },

        add: function(markerId, arObject){ if (!this.exists(markerId)) arr[markerId] = arObject; },

        remove: function(markerId){
            if (this.exists(markerId)){
                arr[markerId].onDelete();
                arr[markerId] = undefined;
            }
        }
    };
}


export { ARObject3D, ARObjectCollection, ARObject, ARObjectType, ARObject3DAnimated, ARObject3DImage };