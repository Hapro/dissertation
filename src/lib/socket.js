//  Author: Mikiel Agutu (except where otherwise explicitly stated)

let socket = null;
let spoof = false;

//The Socket class wraps socket.io for use in ES6 modules.
//IT can be mocked out in error scenarios/for testing
//If socket.io doesn't work use spoof
try{
    socket = io();
}catch(err){
    socket = null;
    spoof = true;
}

let Socket = {};

//If we are spoofing the socket always return successful data
if(spoof){
    Socket = {
        on: (name, successCallback, errorCallback) => {
            if(name === 'joinArGameResponse'){
                successCallback({models: [{filename: 'dog', colour: 'white', markerId: 0}]});
            }

            if(name =='startArGameResponse'){
                successCallback({password:'spoofPass'});
            }
        },
        emit: (name, data, errorCallback) => {
        }
    }
}
else{ //Use socket.io to connect to server
    Socket = {
        on: (name, successCallback, errorCallback) => {
            if(socket !== null){
                socket.on(name, successCallback);
            }
            else{
                errorCallback();
            }
        },
        emit: (name, data, errorCallback) => {
            if(socket !== null){
                socket.emit(name, data);
            }
            else{
                errorCallback();
            }
        }
    }
}

export default Socket;