//  Author: Mikiel Agutu (except where otherwise explicitly stated)

import React from 'react';
import { render } from 'react-dom';
import JSARToolkitEngine from './lib/AREngine/JSARToolkitEngine';
import WebcamCapture from './lib/WebcamCapture/WebcamCapture';
import App from './gui/App';
import { Provider } from 'react-redux'
import Store from './Store';
import Ar3DScene from './Ar3DScene';
import ActionCreators from './actionCreators/ActionCreators';

//Entry point of application

//Render the React app
render(
  <Provider store={Store}>
    <App />
  </Provider>,
  document.getElementById('app')
);

//Set width and height of webcam capture service
const width = 640;//document.getElementById('arDisplay').getBoundingClientRect().width;
const height = 480;//width * 0.75;
const webcamCapture = WebcamCapture();

//Create the webcam canvas
webcamCapture.createWebcamCanvas(width, height);

//Create the JSARToolkit engine
const arEngine = JSARToolkitEngine(30, width, height, webcamCapture.Canvas());
const ar3DScene = Ar3DScene(arEngine);
arEngine.initializeThreeCamera(ar3DScene.Camera());

//Detect the video sources, dispatching actions upon completion
webcamCapture.detectVideoSources(() => {
  Store.dispatch(ActionCreators.initializeWebcamCapture(webcamCapture));
  Store.dispatch(ActionCreators.initializeArDisplay(width, height));
  Store.dispatch(ActionCreators.initializeArEngine(arEngine));
  Store.dispatch(ActionCreators.initializeAr3DScene(ar3DScene));
  tick();
});

let mouseDown = false;

//On mouse move and down events - detect if mouse is hovering over an object
//or has clicked on an object, and act accordingly
ar3DScene.ThreeRenderer().domElement.onmousedown = (e) => {
  //e.preventDefault();
  const renderer = ar3DScene.ThreeRenderer().domElement;

  //Calculate mouse position in normalized device coordinates
  const rawx = e.clientX;
  const rawy = e.clientY;
  let offsetX = 0;
  let offsetY = 0;
  let el = renderer;
  
  do{
    offsetX += el.offsetLeft;
    offsetY += el.offsetTop;
    el = el.offsetParent;
  }while(el !== null);

  const x = rawx - offsetX;
  const y = rawy - offsetY;

  //Ray casting to check intersections
  const obj = ar3DScene.pointIntersectsObjects(( x / renderer.clientWidth ) * 2 - 1, - (y / renderer.clientHeight ) * 2 + 1);
  Store.dispatch(ActionCreators.setArObject(obj));
}
ar3DScene.ThreeRenderer().domElement.onmousemove = (e) => {
  //e.preventDefault();
  const renderer = ar3DScene.ThreeRenderer().domElement;

  //Calculate mouse position in normalized device coordinates
  const rawx = e.clientX;
  const rawy = e.clientY;
  let offsetX = 0;
  let offsetY = 0;
  let el = renderer;
  
  do{
    offsetX += el.offsetLeft;
    offsetY += el.offsetTop;
    el = el.offsetParent;
  }while(el !== null);

  const x = rawx - offsetX;
  const y = rawy - offsetY;

  const obj = ar3DScene.pointIntersectsObjects(( x / renderer.clientWidth ) * 2 - 1, - (y / renderer.clientHeight ) * 2 + 1);
  Store.dispatch(ActionCreators.highlightArObject(obj));
}

ar3DScene.ThreeRenderer().domElement.onmousedown = (e) => {
  //e.preventDefault();
  const renderer = ar3DScene.ThreeRenderer().domElement;

  //Calculate mouse position in normalized device coordinates
  const rawx = e.clientX;
  const rawy = e.clientY;
  let offsetX = 0;
  let offsetY = 0;
  let el = renderer;
  
  do{
    offsetX += el.offsetLeft;
    offsetY += el.offsetTop;
    el = el.offsetParent;
  }while(el !== null);

  const x = rawx - offsetX;
  const y = rawy - offsetY;

  const obj = ar3DScene.pointIntersectsObjects(( x / renderer.clientWidth ) * 2 - 1, - (y / renderer.clientHeight ) * 2 + 1);
  Store.dispatch(ActionCreators.selectArObject(obj));
};
ar3DScene.ThreeRenderer().domElement.onmousedown = (e) => {
  //e.preventDefault();
  const renderer = ar3DScene.ThreeRenderer().domElement;

  //Calculate mouse position in normalized device coordinates
  const rawx = e.clientX;
  const rawy = e.clientY;
  let offsetX = 0;
  let offsetY = 0;
  let el = renderer;
  
  do{
    offsetX += el.offsetLeft;
    offsetY += el.offsetTop;
    el = el.offsetParent;
  }while(el !== null);

  const x = rawx - offsetX;
  const y = rawy - offsetY;

  const obj = ar3DScene.pointIntersectsObjects(( x / renderer.clientWidth ) * 2 - 1, - (y / renderer.clientHeight ) * 2 + 1);
  Store.dispatch(ActionCreators.setArObject(obj));
};


const FPS = 20;

function tick(){
  //requestAnimationFrame(tick);
  webcamCapture.updateCanvasWithWebcamData();
  //if(webcamCapture.webcamReadyToCapture()) arEngine.run(webcamCapture.getImageDataFromCanvas());
  if(webcamCapture.webcamReadyToCapture()){
    arEngine.run();
  }
  ar3DScene.render();

  setTimeout(tick, 1000/FPS);
}