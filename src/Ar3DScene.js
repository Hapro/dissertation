//  Author: Mikiel Agutu (except where otherwise explicitly stated)

import ThreeUtils from './lib/ThreeUtils/ThreeUtils';
import { ARObject, ARObject3D, ARObject3DAnimated, ARObject3DImage } from './lib/AREngine/ARObjects';
import * as THREE from './lib/three';
import { ARObjectType } from './lib/AREngine/ArObjects';

//Augmened 3d scene

const Ar3DScene = (arEngine) => {
    //Create Three.js scene and renderer
    const threeUtils = ThreeUtils();
    const threeScene = threeUtils.createBasicScene(arEngine.DisplayWidth(), arEngine.DisplayHeight());
    const threeRenderer = threeUtils.createRenderer(arEngine.DisplayWidth(), arEngine.DisplayHeight(), 0x000000);
    //Clock for animation
    const clock = { threeClock: new THREE.Clock(), delta: 0 };

    return{
        Camera: () => { return threeScene.Camera(); },
        ThreeRenderer: () => { return threeRenderer; },

        //Add 3D image to AR tracking system
        addImageToArTrackingSystem: function(markerID, data, onSuccess = () => {}, onError = (e) => {}){
            this.loadArObjectImage(data, (arObject3D)=>{
                this.addArObject3DToThreeScene(arObject3D);
                arEngine.addArObject(markerID, arObject3D);
                onSuccess(arObject3D);
            });
        },

        //Add object to AR tracking system
        addObjectToArTrackingSystem: function(markerID, model, onSuccess = () => { console.log('Added object to AR engine successfully'); }, onError = (e) => {console.log(e)}){
            const cb = (arObject3D) => {
                this.addArObject3DToThreeScene(arObject3D);
                arEngine.addArObject(markerID, arObject3D);
                onSuccess(arObject3D);
            };
            
            try{ //Switch on object type and do what is necessary based on it
                if(model.custom){
                    switch(model.type){
                        case ARObjectType.AR_OBJECT_3D:
                            this.parseArObject3DFromJsonData(model.filename, model.base64JsonData, cb);
                            break;
                        case ARObjectType.AR_OBJECT_3D_ANIMATED:
                            this.parseArObjectAnimated3DFromJsonData(model.filename, model.base64JsonData, cb);
                            break;
                    }
                }
                else{
                    switch(model.type){
                        case ARObjectType.AR_OBJECT_3D:
                            this.loadArObject3D(model.filename, cb);
                            break;
                        case ARObjectType.AR_OBJECT_3D_ANIMATED:
                            this.loadArObject3DAnimated(model.filename, cb);
                            break;
                        case ARObjectType.AR_OBJECT_3D_IMAGE:
                            this.loadArObjectImage(model.filename, model.data, cb);
                            break;
                    }
                }
            }
            catch(error){
                onError(error);
            }
        },

        //Parse animated ar object from JSON data
        parseArObjectAnimated3DFromJsonData: function(name, base64JsonData, callback){
            threeUtils.parseJsonToModel(base64JsonData, (geometry, materials) => {
                materials.forEach(material => { material.skinning = true; });

                const character = new THREE.SkinnedMesh(geometry,
                        new THREE.MeshFaceMaterial(materials));

                const mixer = new THREE.AnimationMixer(character);
                const animations = [];

                //Set up each animation saved in the geometry
                geometry.animations.map((anim, i) => {
                    animations[i] = mixer.clipAction(anim);
                    animations[i].setEffectiveWeight(1);
                    animations[i].enabled = true;
                    animations[i].clampWhenFinished = true;
                    animations[i].setLoop(THREE.LoopRepeat);
                    animations[i].name = anim.name;
                });

                const arObj = ARObject3DAnimated(animations, clock, mixer, name, character);
                callback(arObj);
            });
        },

        //Parse an unanimated 3d object from JSON data
        parseArObject3DFromJsonData: function(name, base64JsonData, callback){
            threeUtils.parseJsonToModel(base64JsonData, (geometry, materials) => {
                const object = threeUtils.createMeshObject(geometry, threeUtils.createMeshFaceMaterial(materials));
                const arObj = ARObject3D(name, object, threeScene);
                callback(arObj);
            });
        },

        //Ad ar object to three js scene
        addArObject3DToThreeScene: function(arObject3D){
            threeScene.addObjectToThreeScene(arObject3D.Object());
            arObject3D.onDelete = () => { threeScene.removeObjectFromThreeScene(arObject3D.Object()); };
        },

        //Load an AR object 
        loadArObject3D: function(filename, callback){
            threeUtils.loadModel(filename, (geometry, materials) => {
                const object = threeUtils.createMeshObject(geometry, threeUtils.createMeshFaceMaterial(materials));
                const arObj = ARObject3D(filename, object, threeScene);
                callback(arObj);
            });
        },

        //Code based on
        //https://github.com/arturitu/threejs-animation-workflow/blob/master/js/main.js
        //http://unboring.net/workflows/animation.html
        loadArObject3DAnimated: function(filename, callback){
            threeUtils.loadModel(filename, (geometry, materials) => {
                materials.forEach(material => { material.skinning = true; });

                const character = new THREE.SkinnedMesh(geometry,
                        new THREE.MeshFaceMaterial(materials));

                const mixer = new THREE.AnimationMixer(character);
                const animations = [];

                geometry.animations.map((anim, i) => {
                    animations[i] = mixer.clipAction(anim);
                    animations[i].setEffectiveWeight(1);
                    animations[i].enabled = true;
                    animations[i].clampWhenFinished = true;
                    animations[i].setLoop(THREE.LoopRepeat);
                    animations[i].name = anim.name;
                });

                const arObj = ARObject3DAnimated(animations, clock, mixer, filename, character);
                callback(arObj);
            });
        },

        loadArObjectImage: function(name, imageData, callback){
            threeUtils.loadTextureFromImageData(imageData, (texture) => {
                const plane = threeUtils.createTexturedPlane(texture);
                const arObject3D = ARObject3DImage(name, plane);
                callback(arObject3D);
            });

        },

        render: function(){
            //update clock
            clock.delta = clock.threeClock.getDelta();
            //render scence
            threeScene.render(threeRenderer);
        },

        //Returns single object in scene that intersect with point
        pointIntersectsObjects: (x, y) => {
            const raycaster = new THREE.Raycaster();
            //Create a new vector2 with the x and y co-ordes
            const point = new THREE.Vector2(); 

            point.x = x;
            point.y = y;

            //Use Three.js ray caster with teh point and the camera
            raycaster.setFromCamera( point, threeScene.Camera());

            const intersects = raycaster.intersectObjects(threeScene.Scene().children, true);
            const objs = arEngine.ArObjectCollection().getAll();

            //Iterate through all intersected objects
            for (let i = 0; i < intersects.length; i++ ) {
                //Iterate through all objects in the AR scene
                for(let p = 0; p < objs.length; p++){
                    const obj = objs[p].Object();
                    
                    //See if this object is a part of an Ar object. If it ise return the parent object
                    if(obj.children.length > 0){   
                        if(obj.children[0] === intersects[i].object){
                            return objs[p];
                        }
                    }

                    if(objs[p].Object() === intersects[i].object){
                        return objs[p];
                    }
                } 
            }
        }
    };
}

export default Ar3DScene;