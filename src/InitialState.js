//  Author: Mikiel Agutu (except where otherwise explicitly stated)

const AppStates = {
    SELECT_WEBCAM: 'SELECT_WEBCAM',
    SETUP_GAME: 'SETUP_GAME',
    MAIN_STATE: 'MAIN_STATE',
    JOIN_GAME: 'JOIN_GAME',
    JOIN_GAME_REFUSED: 'JOIN_GAME_REFUSED',
    JOIN_OR_START_GAME: 'JOIN_OR_START_GAME'
};

const InitialState = {
    arDisplayData:{
        ready: false,
        displayWidth: 0,
        displayHeight: 0
    },
    arEngineSync: 0,
    webcamCapture: null,
    arEngine: null,
    ar3DScene: null,
    appState: AppStates.SELECT_WEBCAM,
    models: [],
    maxModels: 32,
    gameServerPassword: '',
    error: '',
    arObject: null,
    highlightedArObject: null
};

export default InitialState;
export { AppStates };