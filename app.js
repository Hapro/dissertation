//  Author: Mikiel Agutu (except where otherwise explicitly stated)

var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var port = process.env.PORT || 3000;
var passwordNonce = Math.floor((Math.random() * 1000) + 100);

app.get('/', function (req, res) {
  res.sendfile('./build/index.html');
});

app.use(express.static('build'));

http.listen(port, function(){
  console.log('listening on *:' + port);
});

io.on('connection', function(socket){
  socket.on('startArGame', function(data){
    //start the game
    var password = startArGame(data.models);

    //Return the password to the client
    socket.emit('startArGameResponse', {password: password});
  });

  socket.on('joinArGame', function(data){
    var arGame = joinArGame(data.password, socket);

    //Return the game to the client that requested it
    socket.emit('joinArGameResponse', arGame);
  });
});

var arGames = [];

function startArGame(models){
  var password = "" + passwordNonce;
  passwordNonce++;
  arGames.push(ArGame(password, models));
  return password;
}

function joinArGame(password, socket){
  for(var i = 0; i < arGames.length; i++){
    if(arGames[i].password === password){
      arGames[i].playerSockets.push(socket);
      return arGames[i].dataToSend;
    }
  }

  return null;
}

//Define an 3d ar model
function Model(name, type, markerId){
  return{
    name: name,
    type: type,
    markerId: markerId
  };
}

//Define an ar game session that users can connect to
function ArGame(password, models){
  return{
    password: password,
    models: models,
    playerSockets: [],
    dataToSend: { password: password, models: models }
  };
}