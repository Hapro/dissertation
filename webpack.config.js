var path = require("path");

//webpack-dev-server:
//Bundle will be served from memory from src folder
//Static assets will be served from the build folder
//
//Contents of build folder is copied in-memory to src, 
//so stuff looking for static assets ought to consider 
//them to be located in src, src/res, etc

module.exports = {
    entry: "./src/entry.js",
    output: {
        path: path.resolve(__dirname, "build"),
        filename: "bundle.js",
        publicPath: "/src/"
    },
    module: {
        loaders: [
            {
                test: /\.css$/,
                loader: "style!css"
            },
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel',
                query: { presets: ['es2015', 'stage-2'] }
            }
        ]
    }
};