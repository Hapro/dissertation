## Dissertation ##

**To run**
Ensure Node.js is installed.

Open the command line and type

    npm install

To run the dev server type

    npm run devServer

Then navigate to localhost:8080 in your browser.

**To deploy**

To deploy the application for production purposes

    webpack

This will produce a `bundle.js` in the `build` folder. Serving this file, along with the `index.html` file is all that is necessary for making the application work. However I recommend doing it like this.

Type

    npm start

In the command line. Next ensure the reference to the bundle in `index.html` is correct. It should be

    src="./bundle.js"
not 

    src="./src/bundle.js"

The app should then be running on localhost:3000.

**Credits**

Chess pieces by cemkalyoncu (http://www.blendswap.com/blends/view/55719). Licensed under https://creativecommons.org/licenses/by/3.0/
Dragon by PigArt (http://www.blendswap.com/blends/view/77143). Licensed under https://creativecommons.org/licenses/by-nc/3.0/

All npm packages installed inside the `node_modules` folder or listed in the `package.json` file were not created by me. These are external libraries under
various open source licences which are available on their respective online repositories.

JsArToolkit, Js-ArUco, OpenCv, Express, Socket.io, Karma, and Three.js are used by this project. None of these libraries were created by me,
and are used under the various licences found in their source files.

**Further reading**

To familiarise yourself with the technologies used in this project the following links might be helpful:

https://webpack.github.io/
https://facebook.github.io/react/
http://redux.js.org/docs/introduction/
https://www.tutorialspoint.com/nodejs/
https://socket.io/