// Karma configuration
// Generated on Mon Dec 12 2016 13:36:44 GMT+0000 (GMT Standard Time)
var webpack = require('webpack');

module.exports = function(config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine'],
    files: [
      'tests.webpack.js'
    ],
    preprocessors: {
      'tests.webpack.js' : ['webpack']
    },
    webpack: {
      devtool: 'inline-source-map',
      module: {loaders: [{ test: /\.js$/, loader: 'babel-loader', exclude: /\/node_modules\//, query: { presets: ['airbnb'] } }]},
      externals: {
        'cheerio': 'window',
        'react/addons': true,
        'react/lib/ExecutionEnvironment': true,
        'react/lib/ReactContext': true
      }
    },
    webpackServer: {
      noInfo: true
    },
    reporters: ['progress'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['Chrome'],
    singleRun: false,
    concurrency: Infinity
  })
}
